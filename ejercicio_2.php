<?php
	require_once 'vendor/autoload.php';
	include_once ('ejercicio_2_config.php');

	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
	$reader->setReadDataOnly(TRUE);
	$spreadsheet   = $reader->load("test.xlsx");
	$worksheet     = $spreadsheet->getActiveSheet();
	// Get the highest row and column numbers referenced in the worksheet
	$highestRow    = $worksheet->getHighestRow(); // e.g. 10
	$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
	$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

	echo '<table border="1">' . "\n";
	for ($row = 1; $row <= $highestRow; ++$row) {
		echo '<tr>' . PHP_EOL;
		for ($col = 1; $col <= $highestColumnIndex; ++$col) {
			$value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
			echo '<td>' . $value . '</td>' . PHP_EOL;
		}
		echo '</tr>' . PHP_EOL;
	}
	echo '</table>' . PHP_EOL;

	$sql = "";

	for ($row = 2; $row <= $highestRow; ++$row) {
		$sql.="insert into alumnos(nombre,apellido,cedula,matricula,carrera,nacionalidad) values(";
		for ($col = 1; $col <= $highestColumnIndex; ++$col) {
			if($col==6)
				$sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."')";
			else
				$sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."',";
		}
		$sql.=";";
	}

	$resultado= pg_query($conn, $sql);

		if (!$resultado) {
			echo "Ocurrió un error al consultar";
			exit;
		} else {
			echo "Se insertó los datos dentro del PostgreSQL.";
		}
?>